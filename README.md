# NOMAD AI-Toolkit App

This repository contains the NOMAD AI-Toolkit App, which allows for a local deployment of all tools and software also installed in the web-based version of the AI-Toolkit https://nomad-lab.eu/aitoolkit.

To use the App on your local machine clone this repository, and from the root directory:

`docker-compose up`

This will create a container to deploy the AI toolkit, and you can then access the App locally using a browser from the following port http://localhost:8080. The container is isolated from the environment defined on your local machine, and you can use it to deploy all software installed in the AI-toolkit without the need of any local installations. All tutorials can be modified in the toolkit, and modifications can be saved inside the container.  The container can be destroyed using the following command from the root directory:

`docker-compose down`

Once the container is destroyed, all modifications to tutorials are lost, and if you start again the App, you will find all tutorials in their original version. Rebooting your machine will also automatically destroy all open containers.

In case you wish to save some work locally and access it and modify it iteratively, you can use the './work' directory. File saved in the './work' directory is going to be stored on your local machine and will stay there also after the container is destroyed. It is also possible to use the './work' directory to deploy software installed in the AI-Toolkit on your own data. The './work' directory contains an introductory tutorial and can be accessed from the main page of the App clicking on the 'Get to work' icon.`

This repository is periodically updated with newer images. In order to stay up to date with the latest version, you can use git to pull the new version. Before creating a container with the new image using 'docker-compose up', you can deploy the following command to remove the old images from your local storage:

`docker rmi $(docker images | grep 'gitlab-registry.mpcdf.mpg.de/nomad-lab/aitoolkit-app')`

To successfully remove all images, containers should be previously destroyed.
